<!doctype html>
<!--[if lt IE 7 ]> <html class="ie6 lt-ie9"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7 lt-ie9"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8 lt-ie9"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Haste Carpentry and Joinery</title>
        <meta name="keywords" content="carpentry, joinery" />
        <meta name="description" content="Fine carpentry and joinery for the commercial and domestic markets" />

        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Lato:400,900,700'>
        <link rel="stylesheet" type='text/css' href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type='text/css' href="/css/cover.css"> 

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>

        <div class="site-wrapper">

            <div class="site-wrapper-inner">

                <div class="cover-container">

                    <div class="masthead clearfix">
                        <div class="inner">
                            <img src="img/logo.jpg" />
                        </div>
                    </div>

                    <div class="inner cover">
                        <img src="img/bubble.png" class="cover-heading" />
                        <p class="lead">Domestic &amp; Commercial Craftsmanship</p>
                        <p class="lead" id="get-in-touch-button-container">
                            <img src="img/strip.png" class="strip left" />
                            <a href="mailto:enquiries@hastecarpentry.co.uk" class="btn btn-lg btn-default">Get in touch</a>
                            <img src="img/strip.png" class="strip right" />
                        </p>
                    </div>

                    <div class="mastfoot">
                        <div class="inner">
                            <p class="big-tel">
                                <img src="/img/icon-phone.png" />
                                <span>07709 315969</span>
                            </p>
                        </div>
                        <div class="inner">
                            <p>14 Meyel Avenue, Canvey Island, Essex, SS8 8BU<br />T. 01268 695552</p>
                            <p><a href="mailto:enquiries@hastecarpentry.co.uk">enquiries@hastecarpentry.co.uk</a></p>
                        </div>
                        <div class="inner">
                            <a href="http://www.facebook.com/hastecarpentry" target="_blank">
                                <img src="/img/facebook-logo.png" />
                            </a>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    </body>
</html>
